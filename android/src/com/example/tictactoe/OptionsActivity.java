/* - - - - - - - - - - - - - - - - - - - - - -
Name: Lucas Connors
Course: CS 349
Assignment: A4
File: OptionsActivity.java
Note: Some skeleton code taken from Java Android Examples from the course web site
- - - - - - - - - - - - - - - - - - - - - - */

package com.example.tictactoe;

import com.example.tictactoe.MyApplication;

import java.util.Observable;
import java.util.Observer;

import android.os.Bundle;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Button;

public class OptionsActivity extends Activity implements Observer {
	// constants
	private static final int PLAYER_X = 1;
	private static final int PLAYER_O = 2;
	
	private EditText editTxtPlayerX;
	private EditText editTxtPlayerO;
	private RadioButton radioBtnX;
	private RadioButton radioBtnO;
	private Button btnStart;
	private Button btnResetScores;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
    	Log.d("Tic-Tac-Toe", "Options onCreate");
        super.onCreate(savedInstanceState);
        
        // load the base UI
        setContentView(R.layout.activity_options);
        
		// add this activity to the model's list of observers
        MyApplication.model.addObserver(this);
		
		// get the context (must be final to reference inside anonymous object)
		final Context context = this;
		
		// get a reference to widgets
		editTxtPlayerX = (EditText)findViewById(R.id.editTxtPlayerX);
		editTxtPlayerO = (EditText)findViewById(R.id.editTxtPlayerO);
		radioBtnX = (RadioButton)findViewById(R.id.radioBtnX);
		radioBtnO = (RadioButton)findViewById(R.id.radioBtnO);
		btnStart = (Button)findViewById(R.id.btnStart);
		btnResetScores = (Button)findViewById(R.id.btnResetScores);
		
		// controller for the radio buttons
		radioBtnX.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				MyApplication.model.selectStartingPlayer(PLAYER_X);
			}
		});
		radioBtnO.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				MyApplication.model.selectStartingPlayer(PLAYER_O);
			}
		});
		
		// controller for the start button
		btnStart.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// set the player names
				MyApplication.model.playerXStr = editTxtPlayerX.getText().toString();
            	MyApplication.model.playerOStr = editTxtPlayerO.getText().toString();
            	//MyApplication.model.setPlayerName(PLAYER_X, editTxtPlayerX.getText().toString());
        		//MyApplication.model.setPlayerName(PLAYER_O, editTxtPlayerO.getText().toString());
				
				// create intent to request the main activity to be shown
				Intent intent = new Intent(context, MainActivity.class);
				// start the activity
                startActivity(intent); 
                finish();
                // we're done with this activity ...
			}
		});
		
		// controller for the reset scores button
		btnResetScores.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// reset scores
				MyApplication.model.resetScores();
			}
		});
		
		MyApplication.model.setChanged();
		MyApplication.model.notifyObservers();
    }
    
	@Override
	protected void onDestroy() {
		Log.d("Tic-Tac-Toe", "OptionsActivity: onDestroy");

		// remove this activity from the model's list of when destroyed
		MyApplication.model.deleteObserver(this);
		
		super.onDestroy();
	}
    
    @Override
    protected void onPostCreate(Bundle savedInstanceState){
    	super.onPostCreate(savedInstanceState);
    }
    
	@Override
	public void update(Observable observable, Object data) {
	    Log.d("Tic-Tac-Toe", "update Options");
	    
	    // update edit text fields
	    editTxtPlayerX.setText(MyApplication.model.playerXStr);
	    editTxtPlayerO.setText(MyApplication.model.playerOStr);
	    //editTxtPlayerX.setText(MyApplication.model.getPlayerName(PLAYER_X), TextView.BufferType.EDITABLE);
	    //editTxtPlayerO.setText(MyApplication.model.getPlayerName(PLAYER_O), TextView.BufferType.EDITABLE);
	    
	    // update radio buttons
	    switch (MyApplication.model.getStartingPlayer()){
	    	case PLAYER_X:
	    		radioBtnX.setChecked(true);
	    		break;
	    	case PLAYER_O:
	    		radioBtnO.setChecked(true);
	    		break;
	    }
	}
}