/* - - - - - - - - - - - - - - - - - - - - - -
Name: Lucas Connors
Course: CS 349
Assignment: A4
File: MainActivity.java
Note: Some skeleton code taken from Java Android Examples from the course web site
- - - - - - - - - - - - - - - - - - - - - - */

package com.example.tictactoe;

import android.os.Bundle;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.ViewGroup;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MenuItem.OnMenuItemClickListener;

public class MainActivity extends Activity {
	private ToolBarView toolbar;
	private BoardView board;
	private PlayerView playerview;
	private TournamentView tournamentview;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        Log.d("Tic-Tac-Toe", "Main onCreate");
        
        // load the base UI
        setContentView(R.layout.activity_main);
    }
    
    @Override
    protected void onPostCreate(Bundle savedInstanceState){
    		super.onPostCreate(savedInstanceState);
    		
    		Log.d("Tic-Tac-Toe", "onPostCreate");
    		
    		// create the views and add them to the main activity
    		toolbar = new ToolBarView(this, MyApplication.model);
    		ViewGroup v1 = (ViewGroup)findViewById(R.id.mainactivity_1);
    		v1.addView(toolbar);
    		board = new BoardView(this, MyApplication.model);
    		ViewGroup v2 = (ViewGroup)findViewById(R.id.mainactivity_2);
    		v2.addView(board);
    		playerview = new PlayerView(this, MyApplication.model);
    		ViewGroup v3 = (ViewGroup)findViewById(R.id.mainactivity_3);
    		v3.addView(playerview);
    		tournamentview = new TournamentView(this, MyApplication.model);
    		ViewGroup v4 = (ViewGroup)findViewById(R.id.mainactivity_4);
    		v4.addView(tournamentview);
    		
    		// initialize views
    		MyApplication.model.setChanged();
    		MyApplication.model.notifyObservers();
    }
    
	@Override
	protected void onDestroy() {
		Log.d("Tic-Tac-Toe", "Main: onDestroy");

		// remove views from the model's list when destroyed
		MyApplication.model.deleteObserver(toolbar);
		MyApplication.model.deleteObserver(board);
		MyApplication.model.deleteObserver(playerview);
		MyApplication.model.deleteObserver(tournamentview);
		
		super.onDestroy();
	}

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        
		// get the menu item to add a listener
		MenuItem item = menu.findItem(R.id.action_settings);
		
		// get the context (must be final to reference inside anonymous object)
		final Context context = this;
		
		// create the menu item controller to change views
		item.setOnMenuItemClickListener(new OnMenuItemClickListener() {
			@Override
			public boolean onMenuItemClick(MenuItem item) {
				// create intent to request the options activity to be shown
				Intent intent = new Intent(context, OptionsActivity.class);
				// start the activity
	            startActivity(intent); 
	            finish();
	            // we're done with this activity ...
	
	            return false;
			}
		});
		
        return true;
    }
}