/* - - - - - - - - - - - - - - - - - - - - - -
Name: Lucas Connors
Course: CS 349
Assignment: A4
File: ToolBarView.java
Note: Some skeleton code taken from Java Android Examples from the course web site
- - - - - - - - - - - - - - - - - - - - - - */

package com.example.tictactoe;

import java.util.Observable;
import java.util.Observer;

import android.content.Context;
import android.util.*;
import android.view.View;
import android.widget.TextView;
import android.widget.Button;
import android.widget.LinearLayout;

public class ToolBarView extends LinearLayout implements Observer {
	private static final int PLAYER_X = 1;
	private static final int PLAYER_O = 2;
	private static final int PLAYER_SELECT = 0;
	private static final int PLAYER_CHANGE = 1;
	private static final int NUM_OF_MOVES = 2;
	private static final int ILLEGAL_MOVE = 3;
	private static final int WINNERX = 4;
	private static final int WINNERO = 5;
	private static final int WINNER_CATSGAME = 6;
	
	private Context context;
	private Model model;
	private Button newGame;
	private TextView messages;
	
	public ToolBarView(Context context, Model m){
		super(context);
		this.context = context;
		
		Log.d("Tic-Tac-Toe", "ToolBarView constructor");
		
		// inflate the view into the display
		View.inflate(context, R.layout.toolbarview, this);
		
		// save the model reference
		model = m;
		model.addObserver(this);
		
		// get button and message textview
		newGame = (Button)findViewById(R.id.btnNewGame);
		messages = (TextView)findViewById(R.id.txtMessages);
		
		// create a controller for the button
		newGame.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// reset the model
				model.gameReset();
			}
		});
	}
	
	// update interface
	public void update(Observable observable, Object data){
		Log.d("Tic-Tac-Toe", "update ToolBarView");
		String msg = context.getResources().getString(R.string.messages);
		switch (model.getGameState()){
			case PLAYER_SELECT:
				msg = context.getResources().getString(R.string.player_select);
				break;
			case PLAYER_CHANGE:
				msg = context.getResources().getString(R.string.player_change);
				break;
			case NUM_OF_MOVES:
				msg = model.getNumTurns() + context.getResources().getString(R.string.num_of_moves);
				break;
			case ILLEGAL_MOVE:
				msg = context.getResources().getString(R.string.illegal_move);
				break;
			case WINNERX:
				if (model.getPlayerName(PLAYER_X).equals("")){
					msg = context.getResources().getString(R.string.playerX);
				}
				else {
					msg = model.getPlayerName(PLAYER_X) + " (" + context.getResources().getString(R.string.playerX) + ")";
				}
				msg += context.getResources().getString(R.string.winner);
				break;
			case WINNERO:
				if (model.getPlayerName(PLAYER_O).equals("")){
					msg = context.getResources().getString(R.string.playerO);
				}
				else {
					msg = model.getPlayerName(PLAYER_O) + " (" + context.getResources().getString(R.string.playerO) + ")";
				}
				msg += context.getResources().getString(R.string.winner);
				break;
			case WINNER_CATSGAME:
				msg = context.getResources().getString(R.string.winner_catsgame);
				break;
		}
		messages.setText(msg);
	}
}