/* - - - - - - - - - - - - - - - - - - - - - -
Name: Lucas Connors
Course: CS 349
Assignment: A4
File: BoardView.java
Note: Some skeleton code taken from Java Android Examples from the course web site
- - - - - - - - - - - - - - - - - - - - - - */

package com.example.tictactoe;

import java.util.Observable;
import java.util.Observer;

import android.content.Context;
import android.util.*;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;

public class BoardView extends RelativeLayout implements Observer {
	// constants
	private static final int NOONE = 0;
	private static final int PLAYER_X = 1;
	private static final int PLAYER_O = 2;
	
	private Model model;
	private Button space[][] = new Button[3][3];
	
	public BoardView(Context context, Model m){
		super(context);
		
		Log.d("Tic-Tac-Toe", "BoardView constructor");
		
		// inflate the view into the display
		View.inflate(context, R.layout.boardview, this);
		
		// save the model reference
		model = m;
		model.addObserver(this);
		
		// get buttons
														// I realize there are probably smarter ways to do this
														// I just don't know Android that well but maybe I'll
														// figure it out later
		space[0][0] = (Button)findViewById(R.id.grid00);
		space[1][0] = (Button)findViewById(R.id.grid10);
		space[2][0] = (Button)findViewById(R.id.grid20);
		space[0][1] = (Button)findViewById(R.id.grid01);
		space[1][1] = (Button)findViewById(R.id.grid11);
		space[2][1] = (Button)findViewById(R.id.grid21);
		space[0][2] = (Button)findViewById(R.id.grid02);
		space[1][2] = (Button)findViewById(R.id.grid12);
		space[2][2] = (Button)findViewById(R.id.grid22);
		
		// create a controller for the button
		for (int row=0; row<3; ++row){
			for (int col=0; col<3; ++col){
				final int thisRow = row;
				final int thisCol = col;
				space[col][row].setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						// do this each time the button is clicked
						model.enterThisSpace(thisCol, thisRow);
					}
				});
			}
		}
	}
	
	// update interface
	public void update(Observable observable, Object data){
		Log.d("Tic-Tac-Toe", "update BoardView");
		// place player in spaces
		for (int row=0; row<3; ++row){
			for (int col=0; col<3; ++col){
				space[col][row].setBackgroundResource(android.R.drawable.btn_default);
				switch (model.getPlayerHere(col, row)){
					case NOONE:
						space[col][row].setText("");
						break;
					case PLAYER_X:
						space[col][row].setText("X");
						break;
					case PLAYER_O:
						space[col][row].setText("O");
						break;
				}
			}
		}
		// winner highlighting
		if (model.getWinningRow(0) != -1){
			for (int i=0; i<3; ++i){
				space[model.getWinningCol(i)][model.getWinningRow(i)].setBackgroundColor(getResources().getColor(R.color.green));
			}
		}
	}
}