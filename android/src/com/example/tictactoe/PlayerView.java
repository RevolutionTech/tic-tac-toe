/* - - - - - - - - - - - - - - - - - - - - - -
Name: Lucas Connors
Course: CS 349
Assignment: A4
File: PlayerView.java
Note: Some skeleton code taken from Java Android Examples from the course web site
- - - - - - - - - - - - - - - - - - - - - - */

package com.example.tictactoe;

import java.util.Observable;
import java.util.Observer;

import android.content.Context;
import android.util.*;
import android.view.View;
import android.widget.TextView;
import android.widget.LinearLayout;

public class PlayerView extends LinearLayout implements Observer {
	// constants
	private static final int NOONE = 0;
	private static final int PLAYER_X = 1;
	private static final int PLAYER_O = 2;
	
	private Context context;
	private Model model;
	
	private TextView txtcurrentPlayer;
	
	public PlayerView(Context context, Model m){
		super(context);
		this.context = context;
		
		Log.d("Tic-Tac-Toe", "PlayerView constructor");
		
		// inflate the view into the display
		View.inflate(context, R.layout.playerview, this);
		
		// save the model reference
		model = m;
		model.addObserver(this);
		
		// get currentPlayer TextView
		txtcurrentPlayer = (TextView)findViewById(R.id.txtCurrentPlayer);
	}
	
	// update interface
	public void update(Observable observable, Object data){
		Log.d("Tic-Tac-Toe", "update PlayerView");
		int currentPlayer = model.getCurrentPlayer();
		String msg = context.getResources().getString(R.string.no_starting_player);
		switch (currentPlayer){
			case NOONE:
				if (model.getGameOver()){
					msg = context.getResources().getString(R.string.no_player_game_over);
				}
				break;
			case PLAYER_X:
				msg = context.getResources().getString(R.string.current_player1);
				if (model.getPlayerName(PLAYER_X).equals("")){
					msg += context.getResources().getString(R.string.playerX); 
				}
				else {
					msg += model.getPlayerName(PLAYER_X);
				}
				msg += context.getResources().getString(R.string.current_player2);
				if (!model.getPlayerName(PLAYER_X).equals("")){
					msg += " (" + context.getResources().getString(R.string.playerX) + ")"; 
				}
				break;
			case PLAYER_O:
				msg = context.getResources().getString(R.string.current_player1);
				if (model.getPlayerName(PLAYER_O).equals("")){
					msg += context.getResources().getString(R.string.playerO); 
				}
				else {
					msg += model.getPlayerName(PLAYER_O);
				}
				msg += context.getResources().getString(R.string.current_player2);
				if (!model.getPlayerName(PLAYER_O).equals("")){
					msg += " (" + context.getResources().getString(R.string.playerO) + ")"; 
				}
				break;
		}
		txtcurrentPlayer.setText(msg);
	}
}