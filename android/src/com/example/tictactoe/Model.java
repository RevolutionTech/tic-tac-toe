/* - - - - - - - - - - - - - - - - - - - - - -
Name: Lucas Connors
Course: CS 349
Assignment: A4
File: Model.java
Note: Some skeleton code taken from Java Android Examples from the course web site
- - - - - - - - - - - - - - - - - - - - - - */

package com.example.tictactoe;

import android.util.Log;
import java.util.Observable;
import java.util.Observer;

public class Model extends Observable {
	// constants
	private static final int NOONE = 0;
	private static final int PLAYER_X = 1;
	private static final int PLAYER_O = 2;
	private static final int CATSGAME = 3;
	private static final int PLAYER_SELECT = 0;
	private static final int PLAYER_CHANGE = 1;
	private static final int NUM_OF_MOVES = 2;
	private static final int ILLEGAL_MOVE = 3;
	private static final int WINNERX = 4;
	private static final int WINNERO = 5;
	private static final int WINNER_CATSGAME = 6;

	public String playerXStr;
	public String playerOStr;
	private int holdingSpace[][] = new int[3][3];
	private int startingPlayer;
	private int currentPlayer;
	private int numTurns;
	private int gameState;
	private boolean gameOver;
	private int winningRow[] = new int[3];
	private int winningCol[] = new int[3];
	private int playerXScore;
	private int playerOScore;

	Model(){
        // set player scores to 0
        playerXScore = 0;
        playerOScore = 0;
        
        // set player names to ""
        playerXStr = "";
        playerOStr = "";
		
		// reset to init game
		gameReset();
	}
	
	// returns the given player's name
	public String getPlayerName(int player){
		String playerName = "";
		switch (player){
			case PLAYER_X:
				playerName = playerXStr;
				break;
			case PLAYER_O:
				playerName = playerOStr;
				break;
		}
		return playerName;
	}
	
	// returns the int array of holdingSpace[col][]
	public int [] getHoldingSpaceCol(int col){
		return holdingSpace[col];
	}
	
	// returns the player located at location (col, row)
	public int getPlayerHere(int col, int row){
		return holdingSpace[col][row];
	}
	
	// returns the starting player
	public int getStartingPlayer(){
		return startingPlayer;
	}
	
	// returns the current player
	public int getCurrentPlayer(){
		return currentPlayer;
	}
	
	// returns the number of moves made so far in the game
	public int getNumTurns(){
		return numTurns; 
	}
	
	// returns the current message
	public int getGameState(){
		return gameState;
	}
	
	// returns the gameOver boolean
	public boolean getGameOver(){
		return gameOver;
	}
	
	// returns true if the player selection buttons should be active and false otherwise
	public boolean playerBtnsActive(){
		return (numTurns <= 0 && !gameOver);
	}
	
	// returns the winning row
	public int getWinningRow(int n){
		return winningRow[n];
	}
	
	// returns the winning col
	public int getWinningCol(int n){
		return winningCol[n];
	}
	
	// returns the full winningRow int array
	public int [] getFullWinningRow(){
		return winningRow;
	}
	
	// returns the full winningCol int array
	public int [] getFullWinningCol(){
		return winningCol;
	}
	
	// returns the score for given player
	public int getPlayerScore(int player){
		int score = 0;			// this value will always be replaced because the switch statement will always follow a case
									// this is really just to keep the compiler happy
		switch (player){
			case PLAYER_X:
				score = playerXScore;
				break;
			case PLAYER_O:
				score = playerOScore;
				break;
		}
		return score;
	}
	
	// set the given player's name
	public void setPlayerName(int player, String playerStr){
		switch (player){
			case PLAYER_X:
				playerXStr = playerStr;
				break;
			case PLAYER_O:
				playerOStr = playerStr;
				break;
		}
		setChanged();
		notifyObservers();
	}
	
	// selects the given player as the starting player
	public void selectStartingPlayer(int player){
		if (numTurns <= 0){
			currentPlayer = player;
			startingPlayer = player;
			numTurns = 0;
			gameState = PLAYER_CHANGE;
			setChanged();
			notifyObservers();
		}
	}
	
	// returns true if the space (col, row) is empty and false if taken
	private boolean isSpaceEmpty(int col, int row){
		if (holdingSpace[col][row] == NOONE){
			return true;
		}
		else {
			return false;
		}
	}
	
	// enters the current player into the space (col, row)
	public void enterThisSpace(int col, int row){
		if (!gameOver && isSpaceEmpty(col, row) && currentPlayer != NOONE){
			// enter the current player into this space
			holdingSpace[col][row] = currentPlayer;
			
			// if the game is over, end it
			int winner = whoWon();
			if (winner != NOONE){
				gameEnd(winner);
			}
			// else, continue with the game
			else {
				currentPlayer = 3 - currentPlayer;			// swaps players
				++numTurns;
				gameState = NUM_OF_MOVES;
				setChanged();
				notifyObservers();
			}
		}
		else if (!gameOver && currentPlayer != NOONE){
			gameState = ILLEGAL_MOVE;
			setChanged();
			notifyObservers();
		}
	}
	
	// returns the winning player of row n (returns 0 if no winner)
	private int whoWonRow(int n){
		int player = holdingSpace[0][n];
		if (player == holdingSpace[1][n] && player == holdingSpace[2][n]){
			if (player != NOONE){
				for (int i=0; i<3; ++i){
					winningRow[i] = n;
					winningCol[i] = i;
				}
			}
			return player;
		}
		else {
			return NOONE;
		}
	}
	
	// returns the winning player of col n (returns 0 if no winner)
	private int whoWonCol(int n){
		int player = holdingSpace[n][0];
		if (player == holdingSpace[n][1] && player == holdingSpace[n][2]){
			if (player != NOONE){
				for (int i=0; i<3; ++i){
					winningRow[i] = i;
					winningCol[i] = n;
				}
			}
			return player;
		}
		else {
			return NOONE;
		}
	}

	// returns the winning player of a diagonal (returns 0 if no winner)
	private int whoWonDiag(int n){
		int player = holdingSpace[1][1];
		switch (n){
			case 0:							// top-left to bottom-right diag
				if (player != holdingSpace[0][0] || player != holdingSpace[2][2]){
					player = NOONE;
				}
				else if (player != NOONE){
					winningRow[0] = 0;
					winningCol[0] = 0;
					winningRow[2] = 2;
					winningCol[2] = 2;
				}
				break;
			case 1:							// top-right to bottom-left diag
				if (player != holdingSpace[2][0] || player != holdingSpace[0][2]){
					player = NOONE;
				}
				else if (player != NOONE){
					winningRow[0] = 0;
					winningCol[0] = 2;
					winningRow[2] = 2;
					winningCol[2] = 0;
				}
				break;
		}
		if (player != NOONE){
			winningRow[1] = 1;
			winningCol[1] = 1;
		}
		return player;
	}
	
	// returns true if the grid is full and false otherwise
	private boolean gridFull(){
		for (int row=0; row<3; ++row){
			for (int col=0; col<3; ++col){
				if (holdingSpace[col][row] == NOONE){
					return false;
				}
			}
		}
		return true;
	}
	
	// returns the winning player (1 for X and 2 for O), returns 0 if the game is not over and 3 if catsgame
	private int whoWon(){
		int winner = NOONE;
		for (int i=0; i<3; ++i){
			winner = whoWonRow(i);					// check rows
			if (winner != NOONE) break;
			winner = whoWonCol(i);					// check cols
			if (winner != NOONE) break;
			if (i < 2){
				winner = whoWonDiag(i);				// check diagonals
				if (winner != NOONE) break;
			}
		}
		if (winner == NOONE && gridFull()){			// check catsgame
			winner = CATSGAME;
		}
		return winner;
	}
	
	// ends the game
	private void gameEnd(int winner){
		currentPlayer = NOONE;
		gameOver = true;
		switch (winner){
			case PLAYER_X:
				gameState = WINNERX;
				++playerXScore;
				break;
			case PLAYER_O:
				gameState = WINNERO;
				++playerOScore;
				break;
			case CATSGAME:
				gameState = WINNER_CATSGAME;
				break;
		}
		setChanged();
		notifyObservers();
	}
	
	// resets the game
	public void gameReset(){
		// reset model
		for (int row=0; row<3; ++row){
			for (int col=0; col<3; ++col){
				holdingSpace[col][row] = NOONE;
			}
		}
		for (int i=0; i<3; ++i){
			winningRow[i] = -1;
			winningCol[i] = -1;
		}
		startingPlayer = NOONE;
		currentPlayer = NOONE;
		numTurns = -1;
		gameOver = false;
		
		// set starting game state
		gameState = PLAYER_SELECT;
		setChanged();
		notifyObservers();
	}
	
	// clear scores
	public void resetScores(){
		playerXScore = 0;
		playerOScore = 0;
	}
	
	// Observer methods
	@Override
	public void addObserver(Observer observer) {
		Log.d("Tic-Tac-Toe", "Model: Observer added");
		super.addObserver(observer);
	}

	@Override
	public synchronized void deleteObservers() {
		super.deleteObservers();
	}

	@Override
	public void notifyObservers() {
		Log.d("Tic-Tac-Toe", "Model: Observers notified");
		super.notifyObservers();
	}

	@Override
	protected void setChanged() {
		super.setChanged();
	}

	@Override
	protected void clearChanged() {
		super.clearChanged();
	}
}