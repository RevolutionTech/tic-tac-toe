/* - - - - - - - - - - - - - - - - - - - - - -
Name: Lucas Connors
Course: CS 349
Assignment: A4
File: TournamentView.java
Note: Some skeleton code taken from Java Android Examples from the course web site
- - - - - - - - - - - - - - - - - - - - - - */

package com.example.tictactoe;

import java.util.Observable;
import java.util.Observer;

import android.content.Context;
import android.util.*;
import android.view.View;
import android.widget.TextView;
import android.widget.LinearLayout;

public class TournamentView extends LinearLayout implements Observer {
	// constants
	private static final int PLAYER_X = 1;
	private static final int PLAYER_O = 2;
	
	private Context context;
	private Model model;
	
	private TextView txtPlayerXScore;
	private TextView txtPlayerOScore;
	
	public TournamentView(Context context, Model m){
		super(context);
		this.context = context;
		
		Log.d("Tic-Tac-Toe", "TournamentView constructor");
		
		// inflate the view into the display
		View.inflate(context, R.layout.tournamentview, this);
		
		// save the model reference
		model = m;
		model.addObserver(this);
		
		// get score TextViews
		txtPlayerXScore = (TextView)findViewById(R.id.txtPlayerXScore);
		txtPlayerOScore = (TextView)findViewById(R.id.txtPlayerOScore);
	}
	
	// update interface
	public void update(Observable observable, Object data){
		Log.d("Tic-Tac-Toe", "update TournamentView");
		String playerXLabel = "";
		if (model.getPlayerName(PLAYER_X).equals("")){
			playerXLabel += context.getResources().getString(R.string.playerX); 
		}
		else {
			playerXLabel += model.getPlayerName(PLAYER_X) + " (" + context.getResources().getString(R.string.playerX) + ")";
		}
		String playerOLabel = "";
		if (model.getPlayerName(PLAYER_O).equals("")){
			playerOLabel += context.getResources().getString(R.string.playerO); 
		}
		else {
			playerOLabel += model.getPlayerName(PLAYER_O) + " (" + context.getResources().getString(R.string.playerO) + ")";
		}
		txtPlayerXScore.setText(playerXLabel + ": " + model.getPlayerScore(PLAYER_X));
		txtPlayerOScore.setText(playerOLabel + ": " + model.getPlayerScore(PLAYER_O));
	}
}