/* - - - - - - - - - - - - - - - - - - - - - -
Name: Lucas Connors
Course: CS 349
Assignment: A3
File: ToolBarView.java
Note: Some skeleton code taken from Java Examples from the course web site
- - - - - - - - - - - - - - - - - - - - - - */

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

public class ToolBarView extends JPanel implements IView {
	Model model;

	JTextArea messages;
	
	ToolBarView(){
		// define view		
		this.setLayout(new FlowLayout(FlowLayout.LEFT));
		this.setBackground(Color.GRAY);
		
		// create button
		this.add(Box.createHorizontalStrut(10));
		JButton newGame = new JButton("New Game");
		this.add(newGame);
		this.add(Box.createHorizontalStrut(30));
		
		// create message label
		messages = new JTextArea("Messages...");
		messages.setEnabled(false);
		messages.setSize(new Dimension(180, 20));
		messages.setLineWrap(true);
		messages.setBackground(Color.GRAY);
		this.add(messages);
		
		// add listener
		// (this anonymous class is essentially the controller)
		newGame.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				model.gameReset();
			}
		});
	}
	
	// set model
	public void setModel(Model model){
		this.model = model;
	}
	
	// IView interface
	public void updateView(){
		messages.setText(model.getCurrentMsg());
	}
}
