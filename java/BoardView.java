/* - - - - - - - - - - - - - - - - - - - - - -
Name: Lucas Connors
Course: CS 349
Assignment: A3
File: BoardView.java
Note: Some skeleton code taken from Java Examples from the course web site
- - - - - - - - - - - - - - - - - - - - - - */

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

public class BoardView extends JPanel implements IView {
	// constants
	private static final int NOONE = 0;
	private static final int PLAYER_X = 1;
	private static final int PLAYER_O = 2;
	
	Model model;
	
	JButton space[][] = new JButton[3][3];
	
	BoardView(){		
		// define view
		this.setLayout(new GridLayout(3, 3));
		
		// add spaces
		for (int row=0; row<3; ++row){
			for (int col=0; col<3; ++col){
				space[col][row] = new JButton("");
				space[col][row].setFocusPainted(false);
				space[col][row].setFont(new Font("Dialog", 1, 72));
				this.add(space[col][row]);
			}
		}
		
		// add listeners
		// (these anonymous classes are essentially the controller)
		for (int row=0; row<3; ++row){
			for (int col=0; col<3; ++col){
				final int thisRow = row;
				final int thisCol = col;
				space[col][row].addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						model.enterThisSpace(thisCol, thisRow);
					}
				});
			}
		}
	}
	
	// set model
	public void setModel(Model model){
		this.model = model;
	}
	
	// IView interface
	public void updateView(){
		// place player in spaces
		for (int row=0; row<3; ++row){
			for (int col=0; col<3; ++col){
				space[col][row].setBackground(Color.WHITE);
				switch (model.getPlayerHere(col, row)){
					case NOONE:
						space[col][row].setText("");
						break;
					case PLAYER_X:
						space[col][row].setText("X");
						break;
					case PLAYER_O:
						space[col][row].setText("O");
						break;
				}
			}
		}
		// winner highlighting
		if (model.getWinningRow(0) != -1){
			for (int i=0; i<3; ++i){
				space[model.getWinningCol(i)][model.getWinningRow(i)].setBackground(Color.GREEN);
			}
		}
	}
}
