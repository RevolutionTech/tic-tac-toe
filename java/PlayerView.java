/* - - - - - - - - - - - - - - - - - - - - - -
Name: Lucas Connors
Course: CS 349
Assignment: A3
File: PlayerView.java
Note: Some skeleton code taken from Java Examples from the course web site
- - - - - - - - - - - - - - - - - - - - - - */

import java.awt.*;
import java.awt.event.*;
import java.awt.geom.*;
import java.awt.geom.Ellipse2D;
import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

public class PlayerView extends JPanel implements IView {
	// constants
	private static final int NOONE = 0;
	private static final int PLAYER_X = 1;
	private static final int PLAYER_O = 2;

	Model model;
	
	JButton buttonX;
	JButton buttonO;
	
	PlayerView(){
		// define view
		this.setLayout(new FlowLayout());
		this.setBackground(Color.BLACK);
		
		// add buttons
		buttonX = new JButton("X");
		buttonO = new JButton("O");
		buttonX.setFocusPainted(false);
		buttonO.setFocusPainted(false);
		this.add(buttonX);
		this.add(Box.createHorizontalStrut(100));
		this.add(buttonO);
		
		// add listeners
		// (these anonymous classes are essentially the controller)
		buttonX.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				model.selectStartingPlayer(PLAYER_X);
			}
		});
		buttonO.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				model.selectStartingPlayer(PLAYER_O);
			}
		});
	}
	
	// set model
	public void setModel(Model model){
		this.model = model;
	}
	
	// IView interface
	public void updateView(){
		repaint();
		buttonX.setEnabled(model.playerBtnsActive());
		buttonO.setEnabled(model.playerBtnsActive());
	}
	
	// custom graphics drawing 
    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        Graphics2D g2 = (Graphics2D) g; // cast to get 2D drawing methods
        g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING,  // antialiasing look nicer
        					RenderingHints.VALUE_ANTIALIAS_ON);
        g2.setStroke(new BasicStroke(5)); // pixel thick stroke
        g2.setColor(Color.BLUE); // make it red
        double width = buttonX.getWidth()*1.5;
        double height = buttonX.getHeight()*1.5;
        Ellipse2D.Double circleX = new Ellipse2D.Double(buttonX.getX()-width/6, buttonX.getY()-height/6, width, height);
        Ellipse2D.Double circleO = new Ellipse2D.Double(buttonO.getX()-width/6, buttonO.getY()-height/6, width, height);        
        switch (model.getCurrentPlayer()){
        	case PLAYER_X:
		        g2.draw(circleX);
		        break;
		    case PLAYER_O:
		    	g2.draw(circleO);
		    	break;
		}
    }
}
