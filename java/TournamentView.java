/* - - - - - - - - - - - - - - - - - - - - - -
Name: Lucas Connors
Course: CS 349
Assignment: A3
File: TournamentView.java
Note: Some skeleton code taken from Java Examples from the course web site
- - - - - - - - - - - - - - - - - - - - - - */

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

public class TournamentView extends JPanel implements IView {
	// constants
	private static final int PLAYER_X = 1;
	private static final int PLAYER_O = 2;
		
	Model model;
	
	JLabel scoreX;
	JLabel scoreO;
	
	TournamentView(){
		// define view
		this.setLayout(new FlowLayout(FlowLayout.RIGHT, 10, 10));
		this.setBackground(Color.GRAY);
		
		// create labels
		scoreX = new JLabel("0");
		scoreO = new JLabel("0");
		this.add(Box.createHorizontalStrut(10));
		this.add(new JLabel("<html><font color='white'>Score:</font></html>"));
		this.add(Box.createHorizontalStrut(5));
		this.add(new JLabel("X: "));
		this.add(scoreX);
		this.add(Box.createHorizontalStrut(5));
		this.add(new JLabel("O: "));
		this.add(scoreO);
		this.add(Box.createHorizontalStrut(10));
	}
	
	// set model
	public void setModel(Model model){
		this.model = model;
	}
	
	// IView interface
	public void updateView(){
		scoreX.setText(Integer.toString(model.getPlayerScore(PLAYER_X)));
		scoreO.setText(Integer.toString(model.getPlayerScore(PLAYER_O)));
	}
}
