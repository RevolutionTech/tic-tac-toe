/* - - - - - - - - - - - - - - - - - - - - - -
Name: Lucas Connors
Course: CS 349
Assignment: A3
File: Main.java
Note: Some skeleton code taken from Java Examples from the course web site
- - - - - - - - - - - - - - - - - - - - - - */

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

public class Main extends JPanel {
    public static void main(String[] args) {
    	// set window width and height
    	int win_width = 640;
    	int win_height = 480;
		
        // create the window
        JFrame frame = new JFrame("Tic-Tac-Toe"); // jframe is the app window
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(win_width, win_height); // window size
        //frame.setContentPane(this); // add canvas to jframe
		
		// set layout
		frame.setLayout(new GridBagLayout());
		// create gridbag constraints
		GridBagConstraints gc = new GridBagConstraints();
		
		// add panels to the frame
		gc.fill = GridBagConstraints.HORIZONTAL;
		gc.gridx = 0;
		gc.weightx = 1.0;
		gc.gridy = 0;
		gc.weighty = 0.0;
		gc.anchor = GridBagConstraints.PAGE_START;
		
		JPanel topPanel = new JPanel();
		topPanel.setLayout(new GridBagLayout());
		GridBagConstraints tpgc = new GridBagConstraints();
		
		tpgc.fill = GridBagConstraints.BOTH;
		tpgc.gridx = 0;
		tpgc.weightx = 0.7;
		tpgc.gridy = 0;
		tpgc.weighty = 1;
		ToolBarView toolbar = new ToolBarView();
		topPanel.add(toolbar, tpgc);
		
		tpgc.gridx = 1;
		tpgc.weightx = 0.3;
		TournamentView tournament = new TournamentView();
		topPanel.add(tournament, tpgc);
		
		frame.add(topPanel, gc);
		
		gc.fill = GridBagConstraints.BOTH;
		gc.gridx = 0;
		gc.weightx = 1.0;
		gc.gridy = 1;
		gc.weighty = 1.0;
		BoardView board = new BoardView();
		frame.add(board, gc);
		
		gc.fill = GridBagConstraints.HORIZONTAL;
		gc.gridy = 2;
		gc.weighty = 0.0;
		gc.anchor = GridBagConstraints.PAGE_END;
		PlayerView playerbar = new PlayerView();
		frame.add(playerbar, gc);
		
		// create the model
        Model model = new Model(toolbar, tournament, board, playerbar);
		
		// show frame
		frame.setVisible(true);
    }
}
