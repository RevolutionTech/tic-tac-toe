/* - - - - - - - - - - - - - - - - - - - - - -
Name: Lucas Connors
Course: CS 349
Assignment: A3
File: Model.java
Note: Some skeleton code taken from Java Examples from the course web site
- - - - - - - - - - - - - - - - - - - - - - */

// View interface
interface IView {
	public void setModel(Model model);
	public void updateView();
}

public class Model {
	// constants
	private static final int NOONE = 0;
	private static final int PLAYER_X = 1;
	private static final int PLAYER_O = 2;
	private static final int CATSGAME = 3;
	private static final int V_ALL = 0;
	private static final int V_TOOLBAR = 1;
	private static final int V_TOURNAMENT = 2;
	private static final int V_BOARD = 3;
	private static final int V_PLAYER = 4;

	private int holdingSpace[][] = new int[3][3];
	private int currentPlayer;
	private int numTurns;
	private String currentMsg;
	private boolean gameOver;
	private int winningRow[] = new int[3];
	private int winningCol[] = new int[3];
	private int playerXScore;
	private int playerOScore;
	
	private IView toolbar;
	private IView tournament;
	private IView board;
	private IView player;

	Model(IView toolbar, IView tournament, IView board, IView player){
		this.toolbar = toolbar;
		this.tournament = tournament;
		this.board = board;
		this.player = player;
		this.toolbar.setModel(this);
		this.tournament.setModel(this);
        this.board.setModel(this);
        this.player.setModel(this);
        
        // set player scores to 0
        playerXScore = 0;
        playerOScore = 0;
		
		// reset to init game
		gameReset();
	}
	
	// returns the player located at location (col, row)
	public int getPlayerHere(int col, int row){
		return holdingSpace[col][row];
	}
	
	// returns the current player
	public int getCurrentPlayer(){
		return currentPlayer;
	}
	
	// returns the current message
	public String getCurrentMsg(){
		return currentMsg;
	}
	
	// returns true if the player selection buttons should be active and false otherwise
	public boolean playerBtnsActive(){
		return (numTurns <= 0 && !gameOver);
	}
	
	// returns the winning row
	public int getWinningRow(int n){
		return winningRow[n];
	}
	
	// returns the winning col
	public int getWinningCol(int n){
		return winningCol[n];
	}
	
	// returns the score for given player
	public int getPlayerScore(int player){
		int score = 0;			// this value will always be replaced because the switch statement will always follow a case
									// this is really just to keep the compiler happy
		switch (player){
			case PLAYER_X:
				score = playerXScore;
				break;
			case PLAYER_O:
				score = playerOScore;
				break;
		}
		return score;
	}
	
	// selects the given player as the starting player
	public void selectStartingPlayer(int player){
		if (numTurns <= 0){
			currentPlayer = player;
			numTurns = 0;
			currentMsg = "Change which player starts, or make first move.";
			notifyObserver(V_TOOLBAR);
			notifyObserver(V_PLAYER);
		}
	}
	
	// returns true if the space (col, row) is empty and false if taken
	private boolean isSpaceEmpty(int col, int row){
		if (holdingSpace[col][row] == NOONE){
			return true;
		}
		else {
			return false;
		}
	}
	
	// enters the current player into the space (col, row)
	public void enterThisSpace(int col, int row){
		if (!gameOver && isSpaceEmpty(col, row) && currentPlayer != NOONE){
			// enter the current player into this space
			holdingSpace[col][row] = currentPlayer;
			
			// if the game is over, end it
			int winner = whoWon();
			if (winner != NOONE){
				gameEnd(winner);
			}
			// else, continue with the game
			else {
				currentPlayer = 3 - currentPlayer;			// swaps players
				++numTurns;
				currentMsg = numTurns + " moves";
				notifyObserver(V_ALL);
			}
		}
		else if (!gameOver && currentPlayer != NOONE){
			currentMsg = "Illegal move";
			notifyObserver(V_TOOLBAR);
		}
	}
	
	// returns the winning player of row n (returns 0 if no winner)
	private int whoWonRow(int n){
		int player = holdingSpace[0][n];
		if (player == holdingSpace[1][n] && player == holdingSpace[2][n]){
			if (player != NOONE){
				for (int i=0; i<3; ++i){
					winningRow[i] = n;
					winningCol[i] = i;
				}
			}
			return player;
		}
		else {
			return NOONE;
		}
	}
	
	// returns the winning player of col n (returns 0 if no winner)
	private int whoWonCol(int n){
		int player = holdingSpace[n][0];
		if (player == holdingSpace[n][1] && player == holdingSpace[n][2]){
			if (player != NOONE){
				for (int i=0; i<3; ++i){
					winningRow[i] = i;
					winningCol[i] = n;
				}
			}
			return player;
		}
		else {
			return NOONE;
		}
	}

	// returns the winning player of a diagonal (returns 0 if no winner)
	private int whoWonDiag(int n){
		int player = holdingSpace[1][1];
		switch (n){
			case 0:							// top-left to bottom-right diag
				if (player != holdingSpace[0][0] || player != holdingSpace[2][2]){
					player = NOONE;
				}
				else if (player != NOONE){
					winningRow[0] = 0;
					winningCol[0] = 0;
					winningRow[2] = 2;
					winningCol[2] = 2;
				}
				break;
			case 1:							// top-right to bottom-left diag
				if (player != holdingSpace[2][0] || player != holdingSpace[0][2]){
					player = NOONE;
				}
				else if (player != NOONE){
					winningRow[0] = 0;
					winningCol[0] = 2;
					winningRow[2] = 2;
					winningCol[2] = 0;
				}
				break;
		}
		if (player != NOONE){
			winningRow[1] = 1;
			winningCol[1] = 1;
		}
		return player;
	}
	
	// returns true if the grid is full and false otherwise
	private boolean gridFull(){
		for (int row=0; row<3; ++row){
			for (int col=0; col<3; ++col){
				if (holdingSpace[col][row] == NOONE){
					return false;
				}
			}
		}
		return true;
	}
	
	// returns the winning player (1 for X and 2 for O), returns 0 if the game is not over and 3 if catsgame
	private int whoWon(){
		int winner = NOONE;
		for (int i=0; i<3; ++i){
			winner = whoWonRow(i);					// check rows
			if (winner != NOONE) break;
			winner = whoWonCol(i);					// check cols
			if (winner != NOONE) break;
			if (i < 2){
				winner = whoWonDiag(i);				// check diagonals
				if (winner != NOONE) break;
			}
		}
		if (winner == NOONE && gridFull()){			// check catsgame
			winner = CATSGAME;
		}
		return winner;
	}
	
	// ends the game
	private void gameEnd(int winner){
		currentPlayer = NOONE;
		gameOver = true;
		switch (winner){
			case PLAYER_X:
				currentMsg = "X Wins";
				++playerXScore;
				break;
			case PLAYER_O:
				currentMsg = "O Wins";
				++playerOScore;
				break;
			case CATSGAME:
				currentMsg = "Game over, no winner";
				break;
		}
		notifyObserver(V_ALL);
	}
	
	// resets the game
	public void gameReset(){
		// reset model
		for (int row=0; row<3; ++row){
			for (int col=0; col<3; ++col){
				holdingSpace[col][row] = NOONE;
			}
		}
		for (int i=0; i<3; ++i){
			winningRow[i] = -1;
			winningCol[i] = -1;
		}
		currentPlayer = NOONE;
		numTurns = -1;
		gameOver = false;
		
		// post starting message
		currentMsg = "Select which player starts.";
		notifyObserver(V_ALL);		
	}
	
	// notify the IView observer
	private void notifyObserver(int observer){
		switch (observer){
			case V_ALL:
				notifyObserver(V_TOOLBAR);
				notifyObserver(V_TOURNAMENT);
				notifyObserver(V_BOARD);
				notifyObserver(V_PLAYER);
				break;
			case V_TOOLBAR:
				toolbar.updateView();
				break;
			case V_TOURNAMENT:
				tournament.updateView();
				break;
			case V_BOARD:
				board.updateView();
				break;
			case V_PLAYER:
				player.updateView();
				break;
		}
	}
}
