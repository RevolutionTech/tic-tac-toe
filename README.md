# Tic-Tac-Toe
# Created by: Lucas Connors
# for CS 349 (User Interfaces)

![Tic-Tac-Toe on Java](http://revolutiontech.ca/media/img/tic-tac-toe-java.jpg)
Java desktop client

![Tic-Tac-Toe on Android](http://revolutiontech.ca/media/img/tic-tac-toe-android.jpg)
Android client

***

## About

The classic game Tic-Tac-Toe made for the purposes of demonstrating the Model-View-Controller design pattern. The project was created for an assignment in CS 349 of Spring 2013.

First the project was created in Java for desktops (Linux) and then the project was re-created in Android using the same model code. Behold, the power of MVC!

## Prerequisites

The Java version of Tic-Tac-Toe requires JDK, which you can install on Debian with:

`sudo apt-get install default-jdk`

## Running

Then the Java version of Tic-Tac-Toe can be run with the following command:

`make run`